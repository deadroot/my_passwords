<?php

function my_password($master_password, $login, $domain, $password_length=16, $hash_algo='sha256')
{
	$auth_string = $login.'@'.$domain;
	$hashed_master_password = substr(hash($hash_algo, $master_password, false), 0, $password_length);
	$encrypted_long_password = base64_encode(hash($hash_algo, $auth_string . $hashed_master_password, false) . $hashed_master_password);
	$encrypted_trimmed_password = substr($encrypted_long_password, 0, $password_length);

	return $encrypted_trimmed_password;
}

?>

<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="./crypto-js/crypto-js.js"></script>
</head>
<body>

<button onclick="test_data()">Fill inputs with test data</button>

<form method='GET' action="index.php" >
	Master password: <input type="input" name="master_password" id="master_password" value="<?php echo $_GET['master_password'] ?>"><br>
	Login: <input type="input" name="login" id="login" value="<?php echo $_GET['login'] ?>"><br>
	Domain: <input type="input" name="domain" id="domain" value="<?php echo $_GET['domain'] ?>"><br>
	Password length: <input type="input" name="password_length" id="password_length" value="16"><br>
	Hash algo: <input type="input" name="hash_algo" id="hash_algo" value="sha256"><br>
	<input type="submit" name="submit" value="Generate password by PHP">
</form>
<button onclick="my_password()">Generate password by JS(SHA256)</button><hr>

Password: <div id="my_password"><?php echo my_password($_GET['master_password'], $_GET['login'], $_GET['domain'], $_GET['password_length'], $_GET['hash_algo']) ?></div>


<script type="text/javascript">
function test_data(argument) {
	document.getElementById("master_password").value = '$3cr3t_M4$t2rp4$$w0rd';
	document.getElementById("login").value = 'my_login';
	document.getElementById("domain").value = 'facebook.com';
}

function my_password(password_length=16, hash_algo='SHA256') {

	var auth_string = document.getElementById('login').value + "@" + document.getElementById('domain').value;
	var hashed_master_password = CryptoJS.SHA256(document.getElementById('master_password').value).toString().substr(0,password_length);

	var encrypted_long_password = CryptoJS.enc.Utf8.parse(hashed_master_password);
	var encrypted_long_password_base64 = CryptoJS.enc.Base64.stringify(encrypted_long_password);

	auth_string = auth_string + hashed_master_password; 
	var encrypted_auth_string_and_encrypted_long_password = CryptoJS.SHA256(auth_string);

	var encrypted_auth_string_and_encrypted_long_password_toutf8 = CryptoJS.enc.Utf8.parse(encrypted_auth_string_and_encrypted_long_password+hashed_master_password);
	var encrypted_auth_string_and_encrypted_long_password_base64 = CryptoJS.enc.Base64.stringify(encrypted_auth_string_and_encrypted_long_password_toutf8).toString().substr(0,password_length);
	document.getElementById('my_password').innerHTML = encrypted_auth_string_and_encrypted_long_password_base64;

}
</script>

</body>
</html>