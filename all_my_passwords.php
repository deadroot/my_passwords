<?php

function my_password($master_password, $login, $domain, $password_length=16, $hash_algo='sha256')
{
	$auth_string = $login.'@'.$domain;
	$hashed_master_password = substr(hash($hash_algo, $master_password, false), 0, $password_length);
	$encrypted_long_password = base64_encode(hash($hash_algo, $auth_string . $hashed_master_password, false) . $hashed_master_password);
	$encrypted_trimmed_password = substr($encrypted_long_password, 0, $password_length);

	return $encrypted_trimmed_password;
}

echo my_password('$3cr3t_M4$t2rp4$$w0rd', 'my_login', 'facebook.com');

die();