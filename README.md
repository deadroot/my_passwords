# All my passwords

How it works:
 1. Concatenate login and domain like login + @ + domain for getting auth string;
 2. Take our master_password and hash it with sha-256 and trim it to password lenght;
 3. Crete base64 from hashed master_password from previous step with sha256(auth_string + hashed_master_password) + hashed_master_password 
 4. Trim this long hash to you password length
 5. Done. Now you have a lot unique passwords for all sites and must remember only master_password ;)